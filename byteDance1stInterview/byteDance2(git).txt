Q1:what's the standard process to solve merge conflict. 
if you are in the branch "fix/somebug" and  you found there are some conflicts with "master" branch. 
type the cmd in the pad.

Answer

Usually the ide would mention conflicts once you pull the latest from master branch to update it. So the conflicts are usually 
settled in the IDE. Then proceed to:
git add --all;
git commit -m "message";
git push origin fix/somebug (or origin master - but usually need to the MR/PR)


Q1B
if on another branch and theres existing changes how to prevent conflict

ANSWER
git stash
git pull origin master
git stash apply


Q2: what's the difference between merge and rebase.

ANSWER
merge creates 2 commit ids - one where the merge happens and the other where the conflict is merged;
rebase commits with only 1 commit id - so it makes the commit tree more beautiful.
