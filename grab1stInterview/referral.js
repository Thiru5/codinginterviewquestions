
/*
Grab wants to have a referral programme to incentivise passengers referring new passengers to the platform.
The incentive structure looks like :

Direct Referrals (1st level)         -> 5$
2nd Level                            -> 3$

Eg:
        5$        3$        
John ——> Jim ———> Bob
 |
 |——> Alex ———> Adam
        |
        |———-> Alice

Implement a function returns the total bonus we payout to a user
calculateTotalBonus(name) = totalBonusInteger

Eg method usages:

calculateTotalBonus(john) = 5 * 2 (jim, alex) + 3 * 3 (bob, adam, alice) = 19$
calculateTotalBonus(jim) = 5$ (bob)
calculateTotalBonus(alex) = 5 * 2 (adam, alice) = 10$
*/


//assume name is unique

const data = {
  "john": ["jim", "alex"],
  "jim": ["bob"],
  "alex": ["adam", "alice"]
}

const INCENTIVE_BONUS = [5, 3];


function calculateTotalBonus(name, level = 0){

  let referrals = data[name] //return array
  if(!referrals){
    return 0;
  }
  let numberOfReferrals = referrals.length // jim alex
  const incentiveBonus = INCENTIVE_BONUS[level]; // 5

  //array of names
  let rewards = numberOfReferrals*incentiveBonus // 10
  
  for(let i = 0; i<referrals.length; i++){

    rewards += calculateTotalBonus(referrals[i], level + 1)
  }

  return rewards
}

console.log(calculateTotalBonus("john", 0))
