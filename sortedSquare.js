//Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
//Input: nums = [-7,-3,2,3,11]
//Output: [4,9,9,49,121]

var sortedSquares = function(nums) {
    
    nums.forEach((num, i) => {
        nums[i] = num**2
    })
    
    nums.sort((a,b) => {
        return a - b
    })
    
    return nums
};

