//given an array containing zeroes, remove the zeroes in between and put them at the back of the array.


let moveZeroes = function(nums) {
    for(let i= nums.length-1; i>=0; i--){
        if(nums[i]===0){
            nums.push(0) 
            nums.splice(i,1)
        }
    }
    return nums
};