// Somehow, we’ve come into possession of an alien dictionary, containing words from an alien language for which we don’t know the ordering of the alphabets.

// Challenge
// Write a method to find the correct order of the alphabet in the alien language. It is given that the input is a valid dictionary and there exists an ordering among its alphabets.

// Examples
// Input: ["ba", "bc", "ac", "cab"]
// Output: bac

// Input: ["cab", "aaa", "aab"]
// Output: cab


//use 2 forloops
//iterate thru word and characters and then compare the two string's first characters
//if same == continue and if diff add the both characters as a pair into a stack and both will be a [] hierarchy
//add all to a map and start from the letter with no char after and eliminate one by one.
