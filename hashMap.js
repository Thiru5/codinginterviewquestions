const collection = new Map()

collection.set("Nathan", "555-012")
collection.set("Lily", "554-0345")


console.log(collection.get("Nathan"))


//this will form a hashmap and create a key value lookup table.


// iteration

for(let [key,value] of collection){
    console.log(`${key} = ${value}`)
}

// use the `` and the dynamic operator ${}



// writing the methods on our own

_hash(key) {
  let hash = 0;
  for (let i = 0; i < key.length; i++) {
    hash += key.charCodeAt(i);
  }
  return hash % this.table.length;
}

set(key, value) {
  const index = this._hash(key);
  this.table[index] = [key, value];
  this.size++;
}

get(key) {
  const index = this._hash(key);
  return this.table[index];
}



remove(key) {
  const index = this._hash(key);

  if (this.table[index] && this.table[index].length) {
    this.table[index] = undefined;
    this.size--;
    return true;
  } else {
    return false;
  }
}


