var rotate = function(nums, k) {
    rotations = k>nums.length-1?k%nums.length:k; //if nums.length is less that k then use nums.length instead

    if(rotations !== 0) {
        nums.unshift(...nums.slice(-rotations));//slice the last few values as per k and put it infront
        nums.splice(-rotations, rotations);//remove the last k values
    }
};