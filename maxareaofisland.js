//You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

//The area of an island is the number of cells with a value 1 in the island.

//Return the maximum area of an island in grid. If there is no island, return 0.

var maxAreaOfIsland = function(grid) {
    let result = 0;
    for (let i = 0; i < grid.length; i++) { //traversing the grid
        for (let j =0; j < grid[0].length; j++) {
            if(grid[i][j] == 1) {
                result = Math.max(result, dfs(i, j, 0));//if 1 is detected do dfs
            }
        }
    }
    
    function dfs(row, colum) {
        if(row < 0 || row >= grid.length || colum < 0 || 
           colum >= grid[0].length || grid[row][colum] !== 1) { //check for boundaries and zeros
            return 0;
        }
        grid[row][colum] = -1;
        return 1 + dfs(row - 1, colum) + dfs(row + 1, colum) + dfs(row, colum - 1) + dfs(row, colum + 1);//4 directional
    }
    
    return result;
};
