// given a grid of an image. Given a new colour value. fill the grid from teh starting point only in teh 4 directional manner. If the pixel is not linked to the starting point e.g.0,0 then it shall nto be filled.



var floodFill = function(image, sr, sc, newColor) {
    fill(image, sr, sc, image[sr][sc], newColor);
    return image;
};

var fill = (image, x, y, oldColor, newColor) => {
    if (x < 0 || y < 0 || x >= image.length || y >= image[x].length || //checking if the value is still within boundary
       image[x][y] === newColor || image[x][y] !== oldColor) {//checking if the node is being revisited
        return;
    }
    image[x][y] = newColor;
    fill(image, x + 1, y, oldColor, newColor);//the 4 directions.
    fill(image, x, y + 1, oldColor, newColor);
    fill(image, x - 1, y, oldColor, newColor);
    fill(image, x, y - 1, oldColor, newColor);//recursion filling
}
