const post = [
  {title: 'Post One', body:'This is post one'},
  {title: 'Post Two', body:'This is post two'}
]

function getPosts(){
  setTimeout(() => {
    let output = ''; //block specificity
    posts.forEach((post,index) => {
      output  += `<li>${post.title}<li>`; //append the title to output var
    });
    document.body.innerHTML = output; //insert into body
  }, 1000)
}

function createPost(post, callback){
  setTimeout(()=> {
    posts.push(post);
    callback();//getPost run immediately after the post is created
  }, 2000)//getPost runs along with the createPost so final output takes 3000ms to display
}

// getPosts()

createPost({ title: 'Post Three', body: 'This is post three'}, getPosts) 
//running this you wont be able to see the Post Three title as the create post takes longer then the getPost by 1000ms

//can be handled by callback param getPosts