function reverse(str){
  var newString = ""
  for(var i = str.length - 1; i>=0; i--)
  {
    newString += str[i];
  }
  return newString;
}

function reverseTwo(str){
  let reversed = " "

  for (let character of str){
    reversed = character + reversed;
  }
  return reversed;
}

reverseTwo("Heloo what is up")