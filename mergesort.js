function merge(left, right){
  let results = []

  while(left.length && right.length){
    if(left[0] < right[0]){
      results.push(left.shift())
    }else{
      results.push(right.shift())
    }
  }

  return [...results,...left,...right]; //adds whatever is there in that particular array
}

function mergeSort(arr){
  if(arr.length == 1){
    return arr;
  }

  const center = Math.floor(arr.length/2);

  const left = arr.slice(0, center) // all the way up to but not including center

  const right = arr.slice(center)

  return merge(mergeSort(left) ,mergeSort(right))

}


//always O(n logn) time complexity but O(n) space
