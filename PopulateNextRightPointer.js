 //while the right node not == NULL then print out the node value
    //after print value set the right node === node
    //when right node is null then print # and go down one level
    

var connect = function(root) {
  if (!root || !root.left) {
    return root;
  }

  root.left.next = root.right;
  root.right.next = root.next ? root.next.left : null;

  connect(root.left);
  connect(root.right);
  return root;
};

//Time complexity: O(n)
//Space complexity: O(1)
