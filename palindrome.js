function palindrome(str){
  let reversed = ""
  for (let character of str){
    reversed = character + reversed
  }
  return str === reversed
}


palindrome("abccfba")