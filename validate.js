//binary tree only 2 children
//to the left is less then center
//to the right is more than center
//assign to left and right instead of children.


class Node{

  constructor(){
    this.data = data;
    this.left = null;
    this.right = null;
  }

  insert(value){
    if(value < this.data && this.left){
      this.left.insert(value)
    }else if(value < this.data){
      this.left = new Node(value)
    }else if(value > this.data && this.right){
      this.right.insert(value)
    }else if(value > this.data){
      this.right=new Node(value)
    }
  }


  contains(value){
    if(value === this.data){
      return true
    }

    if(this.data < value && this.right){
      this.right.contains(value)
    }else if(this.data > value && this.left){
      this.left.contains(value)
    }

    return null
  }
}


function validate(node,min = null, max = null){
  if(max !== null && node.data > max){
    return false
  }

  if( min !== null && node.data < min){
    return false;
  }

  if(node.left && !validate(node.left,min, node.data)){
    return false;
  }

  if(node.right && !validate(node.right, node.data, max)){
    return false;
  }

  return true

}