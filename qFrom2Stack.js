class Stack{
  constructor(){
    this.data = []
  }

  push(data){
    this.data.push(data)
  }

  pop(){
   return this.data.pop()
  }

  peek(){
    return this.data[this.data.length - 1]
  }
  
}


class Queue{
  constructor(){
    this.first = new Stack //auto creates two stack when Q is init
    this.second = new Stack
  }

  add(data){
    this.first.push(data);
  }

  remove(){
    while(this.first.peek()){
      this.second.add(this.first.pop())
    }
    const removed = this.second.pop()

    while(this.second.peek){
      this.first.add(this.second.pop())
    }

    return removed;
  }

  peek(){
   return this.first.peek()
  }


}