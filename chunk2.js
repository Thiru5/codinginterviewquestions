 function chunk(array,size){
   const chunked = []
   let index = 0

   while (index < array.length){
     chunked.push(array.slice(index,index + size))
     index+= size
   }

   return chunked
 }

 //slice function uses a range.


 chunk([1,2,3,4], 2)