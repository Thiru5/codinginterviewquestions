// random generator to associate with the different hand signs
// input for player
// options for pvc and cvc
// pvc = 1 computer and then player is given option to choose
// cvc = 2 computer going against each other

//rock beats sci
//sci beats paper
// paper beats rock
let rockMap = new Map()
rockMap.set(1, 1)

let paperMap = new Map()
paperMap.set(0, 1)

let scissorsMap = new Map()
scissorsMap.set(2, 1)


let answerMap = new Map()
answerMap.set("rock", "scissors")
answerMap.set("scissors", "paper")
answerMap.set("paper", "rock")

function newCheckWinner(p1, p2){
  console.log("Player 1 played: " + p1)
  console.log("Player 2 played: " + p2)
  if(p1 === p2){
    console.log("Its a tie")
    return
  }

  if(p2 === answerMap.get(p1)){
    console.log("Player 1 wins")
  } else {
    console.log("Player 2 wins")
  }

  
}


function runGame(){
  let random = Math.floor(Math.random() * 3);
  if(random === 0){
    return 'rock'
  } else if (random === 1) {
    return "scissors"
  } else {
    return "paper"
  }
  
}

function checkWinner(player1, player2){
  let rock = 0
  let scissors = 1
  let paper = 2
  let spock = 3 
  let lizard = 4

  
  console.log("Checking Winner")
  
  if(player1 === rock){
    console.log("Player 1 played rock")
    if(player2 === paper){
      console.log("Player 2 played paper")
      console.log("Player 1 wins")
    } else if (player2 === scissors){
      console.log("Player 2 played scissors")
       console.log("Player 2 wins")
    } else {
      console.log("Player 2 played rock")
      console.log("Its a tie")
    }
  }

  if(player1 === paper){
     console.log("Player 1 played paper")
    if(player2 === scissors){
      console.log("Player 2 played scissors")
      console.log("Player 2 wins")
    } else if (player2 === rock){
        console.log("Player 2 played rock")
       console.log("Player 1 wins")
    } else {
      console.log("Player 2 played paper")
      console.log("Its a tie")
    }
  }

  if(player1 === scissors){
     console.log("Player 1 played scissors")
    if(player2 === rock){
      console.log("Player 2 played rock")
      console.log("Player 1 wins")
    } else if (player2 === paper){
      console.log("Player 2 played paper")
       console.log("Player 2 wins")
    } else {
      console.log("Player 2 played scissors")
      console.log("Its a tie")
    }
  }
}

function main(chosen, playerChosen){
  console.log("1. PvC")
  console.log("2. CvC")

  if(chosen === 1){
    let computerChosen = runGame()
    newCheckWinner(playerChosen, computerChosen)
    
  } else {
    console.log("This is CvC")
    let computerChosen1 = runGame()
    let computerChosen2 = runGame()
    newCheckWinner(computerChosen1,computerChosen2)
    
  }
}


main(1, "rock")
