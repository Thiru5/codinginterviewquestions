function chunk(array, n){
  const chunked = []
  
  for(let element of array){
    const last = chunked[chunked.length - 1]

    if(!last || last.length === n){
      chunked.push([element])
    }else{
      last.push(element)
    }
  }

  return chunked
}


chunk([1,2,3,4], 2)