//Run js file to see answer

const promise = new Promise((resolve, reject) => {

  console.log(1);
 
  resolve();
 
  console.log(2);
 
  reject();
 
 });
 
 setTimeout(() => {console.log(5) },0);
 
 promise.then(() => { console.log(3) })
 
 .then(() => { console.log(6) })
 
 .catch(() => { console.log(7) });
 
 console.log(4);
 
//  Whats the output?