class Node{
    constructor(){
        this.data = data;
        this.next = next;
    }
}
class LinkedList{
    constructor(){
        this.data = data;
        this.head = null;
        this.tail = null;
    }

    clear(){
        this.head = null
    }

    removeFirst(){
        if(!this.head){
            return
        }
        
        if(this.head.next === null){
            this.clear()
        }

        const next = this.head.next
        this.head = next;
    }

    removeLast(){
        if(!this.head){
            return
        }

        if(this.head.next === null){
            this.clear()
        }

        const previous = this.head;
        const node = previous.next;

        while(node.next){
            prev = node
            node = node.next
        }

        prev.next = null

    }

    getAt(n){
        let counter = 0
        node = this.head

        while(node){
            if(counter === n){
                return node.data
            }
            node = node.next
            counter++
        }

        return null
    }



}