//see leet code for question

var solution = function(isBadVersion, n) {
    
    return function(n) {
        let right = n
        let left = 1
        
        while(left < right){
            let mid = Math.floor((left + right)/2)
            if(isBadVersion(mid)){
                right = mid
            }else{
                left = mid + 1
            }
        }
        return right        
    };
};