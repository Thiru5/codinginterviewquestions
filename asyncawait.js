const posts = [
  {title: 'Post One', body:'This is post one'},
  {title: 'Post Two', body:'This is post two'}
]

function getPosts(){
  setTimeout(() => {
    let output = ''; //block specificity
    posts.forEach((post,index) => {
      output  += `<li>${post.title}<li>`; //append the title to output var
    });
    // document.body.innerHTML = output; //insert into body
  }, 1000)
}

function createPost(post, callback){
  return new Promise((resolve,reject)=> {
    setTimeout(()=> {
      posts.push(post);

      const error = false;
      
      if(!error){
        resolve();
      }else{
        reject("Error: Something went wrong")
      }
    }, 2000)//getPost runs along with the createPost so final output takes 3000ms to display
  })
}

// getPosts()

// createPost({ title: 'Post Three', body: 'This is post three'})
//   .then(getPosts)
//   .catch(err => console.log(err));


//running this you wont be able to see the Post Three title as the create post takes longer then the getPost by 1000ms

//can be handled by promise.then(getPosts)

async function init(){
  await createPost({ title: 'Post Three', body: 'This is post three'})//waits for the await line to finish before proceeding

  getPosts()
}

//do async with fetch

async function fetchUsers(){
  const res = await fetch('url string')

  const data = await res.json();//need to map the result

  console.log(data)
}

fetchUsers();