class Node {
  constructor(){
    this.data = data;
    this.next = next;
  }
}

class LinkedList {
  constructor(){
    this.head = null
  }


  insertFirst(data){
    this.head = new Node(data, this.head) // initial head node becomes tthe second node and the set the new node as head
  }

  size(){
    let counter = 0
    let node = this.head
    while(node !== null){
      counter++
      node = node.next
    }
    return counter
  }

  getFirst(){
    // return this.head;
    return this.getAt(0)
  }

  getLast(){
    // if(!this.head){
    //   return null
    // }

    // let node = this.head
    // while(node){
    //   if(!node.next){
    //     return node;
    //   }
    //   node = node.next
    // }
    return this.getAt(this.size() - 1)
  }

  clear(){
    this.head = null
  }

  removeFirst(){
    if(!this.head){
      return
    }
    this.head = this.head.next

  }

   removeLast(){
     if(!this.head){
       return
     }

     if(this.head.next === null){
       this.head = null
       return
     }

     let prev = this.head
     let node = this.head.next
     while(node.next){
       prev = node
       node = node.next //dont have to do an if statement because loop reaches the end
     } 
     prev.next = null
 
  }

  insertLast(data){
    const last = this.getLast()
    if(last){
      last.next = new Node(n)
    }else{
      this.head = new Node(n)
    }
  }

  getAt(n){
    let node = this.head
    let counter = 0
    while(node){
      if(counter === n){
        return node
      }
      node = node.next
      counter++

    }
    return null
  }

  removeAt(n){
    if(!this.head){
      return
    }

    if(n === 0){
      this.head = this.head.next
      return
    }


    const previous = this.getAt(n-1)
    if(!previous || !previous.next){
      return
    }

    previous.next = previous.next.next

  }

  insertAt(data, n){
    if(!this.head){
      this.head = new Node(data)
      return
    }

    if(n === 0){
      this.head = new Node(data, this.head)
      return
    }

    const previous = this.getAt(n-1) || this.getLast()
    const node = new Node(data,previous.next)
    previous.next = node

  }


}


function midpoint(list){
  let slow = list.head
  let fast = list.head

  while(fast.next & fast.next.next){//slow is moving half as fast as the fast pointer
    slow = slow.next
    fast = fast.next.next
  }

  return slow 

  
}