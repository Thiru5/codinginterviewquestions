//given an arr of integers(negative and positive) square each int and sort the array


 var sortedSquares = function(nums) {
    for(let i = 0; i<nums.length; i++){
       let temp = (nums.shift())**2
       nums.push(temp)
    }
    
    return nums.sort(function(a, b){return a - b})
};