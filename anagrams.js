//check if two strings are the same

function checkAnagram(strA, strB){
  const charsA = createCharsMap(strA)
  const charsB = createCharsMap(strB)

  if(Object.keys(charsA).length != Object.keys(charsB).length){
    return false
  }

  for (let char in charsA){
    if(charsA[char]!== charTwo[char]){
      return false
    }
  }
  return true
}


function createCharsMap(string){
  const charMap = []
  string = string.replace(/[^\w]/g,'').toLowerCase()

  for(let char of string){
    charMap[char] = charMap[char] + 1 || 1
  }

  return charMap
}