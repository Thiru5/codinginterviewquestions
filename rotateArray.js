//question in Leetcode
//rotate array k number of times
//0(1) solution

var rotate = function(nums, k) {
    k = k % nums.length
    nums.unshift(...nums.splice(-k));//splice is a faster function than pop?

};



//splice is used to remove/replace a certain element in the array based on an index