const posts = [
  {title: 'Post One', body:'This is post one'},
  {title: 'Post Two', body:'This is post two'}
]

function getPosts(){
  setTimeout(() => {
    let output = ''; //block specificity
    posts.forEach((post,index) => {
      output  += `<li>${post.title}<li>`; //append the title to output var
    });
    // document.body.innerHTML = output; //insert into body
  }, 1000)
}

function createPost(post, callback){
  return new Promise((resolve,reject)=> {
    setTimeout(()=> {
      posts.push(post);

      const error = false;
      
      if(!error){
        resolve();
      }else{
        reject("Error: Something went wrong")
      }
    }, 2000)//getPost runs along with the createPost so final output takes 3000ms to display
  })
}

// getPosts()

// createPost({ title: 'Post Three', body: 'This is post three'})
//   .then(getPosts)
//   .catch(err => console.log(err));


//running this you wont be able to see the Post Three title as the create post takes longer then the getPost by 1000ms

//can be handled by promise.then(getPosts)

const promise1 = Promise.resolve('Hello World')
const promise2 = 10;
const promise3 = new Promise((resolve, reject)=>
  setTimeout(resolve, 2000, 'Goodbye')
)
const promise4 = fetch('https://jsonplaceholder.typicode.com/todos/1').then(res => res.json())

Promise.all([promise1, promise2, promise3, promise4]).then((values) => console.log(values));//this will take however long the longest promise takes to resolve, in this case 2000
