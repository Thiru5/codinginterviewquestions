

//basecase row === n then we have hit the end of our problem
//n is the end of row then can print
// do a matrix
// do iterative then do recursiveSteps


function recursiveSteps(n, row = 0, stair = []){
  if(n === row){
    return
  }

  if(n == stair.length){
    console.log(stair)
    return recursiveSteps(n ,row + 1)
  }

  if(stair.length <= row){
    stair += '#'
  }else{
    stair += ')'
  }
  recursiveSteps(n,row,stair)
}


recursiveSteps(4)