// there are an 2-dimension matrix，and merge them togeher as an ascending order one-dimension array. every row of the matrix is in an ascending 
// order the time complexity of your approach is O(n*m);

// example: [[1,3,5,19],[2,3,4,10],[8]] => [1,2,3,3,4,5,8,10,19]


// function sortArrays(matrix){

//        //to be completed 

// }

// console.log(sortArrays([[1,3,5,19],[2,3,4,10],[8]]))


function merge(left, right){
    let results = []
  
    //there's a better way to do this
    while(left.length && right.length){
      if(left[0] < right[0]){
        results.push(left.shift())
      }else{
        results.push(right.shift())
      }
    }
  
    return [...results,...left,...right]; //adds whatever is there in that particular array
  }
  
  function sortArrays(arr){
    if(arr.length == 1){
      return arr;
    }

    while(arr.length - 1){
        //this is the key line, the first two elements are passed into left and right respectively into merge f(n)
        // the returned arr from merge f(n) is pushed/added into the existing arr so that it can go thru the loop again and be merged
        arr.push(merge(arr.shift(), arr.shift()))
    }

    // return arr[0] to match the output of the question
    return arr[0]
  
  }

  console.log(sortArrays([[1,3,5,19],[2,3,4,10],[8]]))
