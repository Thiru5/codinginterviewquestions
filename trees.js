class Node{
  constructor(){
    this.data = data
    this.children = []
  }


  add(data){
    this.children.push(new Node(data))
  }

  remove(data){
    //use filter function
    this.children.filter(node => {
      return node.data !== data
    })
  }
  
}


class Tree{
 constructor(){
   this.root = null
  } 
//cant do insert or remove for a tree
//must specify which node exactly to manipulate
//any param input into the traverseBf is a function for each node
  traverseBF(func){
    let arr = [this.root]

    while(arr.length){
      const node = arr.shift();
      
      arr.push(...node.children)
      func(node)

    }
  }  


  traverseDF(func){
    let arr = [this.root]

    while(arr.length){
      const node = arr.shift()

      arr.unshift(...node.children)
      func(node)
    }
  }
}



function levelWidth(root){
  let counters = [0]
  let arr = [root, 's']
  let level = 1
  //add somesort fo flag to notice if the traversal has alrdy processed it 
  while(arr.length > 1){
    const node = arr.shift()

    if(node === 's')
    {
      counters.push(0);
      arr.push('s')
    }else{
      arr.push(...node.children);
      counters[counter.length - 1]++
    }
  }
  return counters

}