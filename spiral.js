function spiral(n){
    //this whole solution is based on the fact that the 

    const results = []

    for(let i = 0; i< n; i++){
        results.push([])
    }

    let counter = 1 
    let startCol = 0
    let endCol = n - 1
    let endRow = n - 1
    let startRow = 0

    while(startCol<=endCol && startRow <= endRow){
        for (let i = startCol; i<= endCol; i++){
            results[startRow][i] = counter
            counter ++
        }
        startRow++ // increment startRow to shift the starting point down

        for(let i = startRow; i<=endRow; i++){
            results[i][endCol] = counter
            counter++
        }

        endCol-- //decrementing endcol because the right side of the spiral is complete


        for(let i = endCol; i>=startCol; i--){//backwards iteration
            results[endRow][i] = counter
            counter++
        }

        endRow-- //decrementing the end row as the counters need
        //to go inwards of the spiral


        for(let i = endRow; i >= startRow; i--){
            results[i][startCol] = counter
            counter++
        }
        startCol++ // increment startRow as the counters need to go inwards
    }
    return results
}

spiral(4)